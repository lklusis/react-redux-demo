import React, { Component } from 'react';
import styles from './countriesAutocomplete.less';
import { AutoComplete } from 'components/presentational/vtnlDesign/autocomplete';
import { LabelledInput } from 'components/presentational/vtnlDesign/labelledInput';
import * as reactStringReplace from 'react-string-replace';
import { connect } from 'react-redux'
import _ from 'lodash';


import { fetchDestinations } from 'modules/destinationsAutocomplete';


import { AutoComplete as AntdAutoComplete } from 'antd';
const Option = AntdAutoComplete.Option;

@connect(
    state => ({
        loading: state.destinationsAutocomplete.loading,
        suggestions: state.destinationsAutocomplete.suggestions,
        errorMessage: state.destinationsAutocomplete.errorMessage,
        request: state.destinationsAutocomplete.request,
    })
)

export class CountriesAutocomplete extends Component {

    handleSearch = (queryValue) => {
        if (queryValue.length > 1) {
            const { dispatch } = this.props;
            dispatch(fetchDestinations(queryValue));
        }
    };

    renderOption = (suggestions) => {
        return _.map(suggestions, (item, index) => (
            <Option key={index} text={item.value}>
                {reactStringReplace(item.value, new RegExp(`(${this.props.request})`, 'i'), (match, i) => (
                    <span className={styles.queryMatch}>{match}</span>
                ))}
            </Option>
        ));
    };


    render() {

        const {
            className,
            label,
            placeholder,
            suggestions,
            ...restProps,
          } = this.props;


        return (
            <div className={`${styles.wrapper} ${className}`}>
                <LabelledInput label={label} className={styles.searchFormRow} >
                    <AutoComplete
                        placeholder={placeholder}
                        dataSource={this.renderOption(suggestions)}
                        handleSearch={this.handleSearch}
                        optionLabelProp="text"
                        {...restProps}
                    />
                </LabelledInput>
            </div>
        );
    }
}

export default CountriesAutocomplete;
