import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { innerLayout, AntInput } from './common';
import { signUpUser } from 'modules/auth'
import { Button, Form, Alert } from "antd";
const AntdFormItem = Form.Item;


const validate = values => {
    const errors = {};
    if (values.password !== values.passwordConfirm) {
        errors.password = 'password not match';
    }
    return errors;
}

@connect(
    state => ({ message: state.auth.message })
)
@reduxForm({
    form: 'login',
    validate
})
export default class SignUpForm extends Component {

    signUp(values) {
        const { dispatch } = this.props;
        dispatch(signUpUser(values.username, values.password));
    }

    render() {
        const  {handleSubmit, pristine, reset, submitting, message } = this.props
        return (
            <Form onSubmit={handleSubmit(v => this.signUp(v))}>
                <AntdFormItem {...innerLayout} style={{ visibility: message ? undefined : "hidden" }}>
                    <Alert message={message} type="warning" />
                </AntdFormItem>

                <Field label="Username"
                    name="username"
                    component={AntInput}
                    placeholder="Username"
                />
                <Field
                    label="Password"
                    name="password"
                    component={AntInput}
                    placeholder="Password"
                    type="password"
                />

                <Field
                    label="Repeat password"
                    name="passwordConfirm"
                    component={AntInput}
                    placeholder="Password"
                    type="password"
                />
                <AntdFormItem {...innerLayout}>
                    <Button
                        type="primary"
                        disabled={pristine || submitting}
                        htmlType="submit"
                        style={{ marginRight: "10px" }}
                    >
                        Submit
                    </Button>

                    <Button disabled={pristine || submitting} onClick={reset}>
                        Clear Values
                    </Button>
                </AntdFormItem>
            </Form>
        );
    }

}