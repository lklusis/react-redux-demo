import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { signInUser } from 'modules/auth'
import { innerLayout, AntInput } from './common';
import { Button, Form, Alert } from "antd";

const AntdFormItem = Form.Item;


@connect(
    state => ({ message: state.auth.message })
)
@reduxForm({
    form: 'login',
})
export default class LoginForm extends Component {

    signIn(values) {
        const { dispatch } = this.props;
        const signInAction = signInUser(values.username, values.password);
        dispatch(signInAction);
    }

    render() {
        const { handleSubmit, pristine, reset, submitting, message } = this.props

        return (
            <Form onSubmit={handleSubmit(v => this.signIn(v))}>

                <AntdFormItem {...innerLayout} style={{ visibility: message ? undefined : "hidden" }}>
                    <Alert message={message} type="warning" />
                </AntdFormItem>

                <Field label="Username"
                    name="username"
                    component={AntInput}
                    placeholder="Username"
                />
                <Field
                    label="Password"
                    name="password"
                    component={AntInput}
                    placeholder="Password"
                    type="password"
                />
                <AntdFormItem {...innerLayout}>
                    <Button
                        type="primary"
                        disabled={pristine || submitting}
                        htmlType="submit"
                        style={{ marginRight: "10px" }}
                    >
                        Submit
                    </Button>

                    <Button disabled={pristine || submitting} onClick={reset}>
                        Clear Values
                    </Button>
                </AntdFormItem>
            </Form>
        );
    }
}