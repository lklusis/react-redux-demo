import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchQuests, selectQuest } from 'modules/quests'

import { QuestCard } from 'components/presentational/questCard';
import { Spin, Alert } from 'antd';
import { Row, Col } from 'antd';

@connect(
    state => ({
        loading: state.quests.list.loading,
        quests: state.quests.list.questsList,
        errorMessage: state.quests.list.errorMessage,
    })
)
export class Quests extends Component {

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchQuests());
    }

    onQuestSelected(event, id) {
        const { dispatch } = this.props;
        dispatch(selectQuest(id));
    }

    render() {
        const { quests, errorMessage, loading } = this.props;
        return (
            <div>
                {loading && <Spin size="large" />}
                {errorMessage && <Alert type="error" message={errorMessage} />}
                <Row>
                    {quests.map(quest => {
                        return <Col key={quest.id} xs={24} sm={12} lg={8} xl={6}>
                            <QuestCard onClick={(e, id) => { this.onQuestSelected(e, id) }} {...quest} ></QuestCard>
                        </Col>
                    })}
                </Row>
            </div>
        );
    }

}