import React, { Component } from 'react';
import styles from './search-form.less';

import { Row, Col } from 'antd';

import { CountriesAutocomplete } from 'components/containers/countriesAutocomplete';

export function SearchForm() {

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Form submitted');
    }

    return (
        <div className={styles.searchForm}>
            <form onSubmit={handleSubmit}>
                <Row>
                    <Col xs={24}>Search form</Col>
                </Row>
                <Row>
                    <CountriesAutocomplete label={'From'} placeholder={'Pick a country'} className={styles.searchFormRow} fixed={true} />
                </Row>
            </form>
        </div>
    );
}