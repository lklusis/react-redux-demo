import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchQuests } from 'modules/quests'

import { QuestCard } from 'components/presentational/questCard';
import { Spin, Alert } from 'antd';
import { Row, Col } from 'antd';

@connect(
    state => ({
        loading: state.quests.loading,
        quests: state.quests.questsList,
        errorMessage: state.quests.errorMessage,
    })
)
export class QuestTeam extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <div>
                Quest Team
            </div>
        );
    }
}