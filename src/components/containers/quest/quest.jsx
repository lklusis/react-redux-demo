import React, { Component } from 'react';
import { connect } from 'react-redux'

import { QuestCard } from 'components/presentational/questCard';
import { Spin, Alert } from 'antd';

import { fetchQuest, selectQuest } from 'modules/quests'

@connect(
    state => ({
        loading: state.quests.selected.loading,
        quest: state.quests.selected.item,
        errorMessage: state.quests.selected.errorMessage,
    })
)
export class Quest extends Component {

    // constructor(props) {
    //     super(props);
    // }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchQuest(this.props.params.id));
    }

    render() {
        const { quest, errorMessage, loading } = this.props;
        
        return (
            <div>
                {loading && <Spin size="large" />}
                {errorMessage && <Alert type="error" message={errorMessage} />}
                <div>{JSON.stringify(this.props.quest, ' ')}</div>
            </div>
        );
    }
}