import React, { Component } from 'react';
import { Card } from 'antd';
import styles from './questCard.less';

export class QuestCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showingMore: false
        };
    }

    toggleShowMore() {
        this.setState({
            showingMore: !this.state.showingMore
        });
    }

    render() {
        const { id, name, description, photoUrl, onClick } = this.props;
        const showingMore = this.state.showingMore;
        const toggleShowMore = this.toggleShowMore.bind(this);

        const onClickHandler = (event) => { onClick(event, id) }

        return (
            <div className={styles.questWrapper}>
                <Card className={styles.card} bodyStyle={{ padding: '10px' }}>
                    <div className={styles.title} onClick={onClickHandler} >{name}</div>
                    <div style={{ backgroundImage: `url(${photoUrl})` }} className={styles.image} onClick={onClickHandler} />
                    <div className={styles.description + ' ' + (!showingMore && styles.showLess)}>
                        {description}
                    </div>
                    <div style={{ textAlign: 'right' }}>

                        <span className={styles.showMoreLink} onClick={toggleShowMore}>
                            {/* TODO: Add Ellipsis (with :after )*/}
                            {showingMore ? 'Show less' : 'Show more'}
                        </span>
                    </div>
                </Card>
            </div>
        )
    }
}
