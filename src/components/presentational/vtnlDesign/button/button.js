import React, { Component } from 'react';
import styles from './button.less';

// TODO: setup babel-plugin-lodash
// https://github.com/lodash/babel-plugin-lodash 
export const Button = (props) => {
  return (
    <div className={styles.buttonWrapper}>
      <button id={props.id} type={props.type} className={styles.button} >
        {props.children}
      </button>
    </div>
  );
}

export default Button;
