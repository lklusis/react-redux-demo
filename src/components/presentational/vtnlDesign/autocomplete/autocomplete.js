import React, { Component } from 'react';
import styles from './autocomplete.less';
import { AutoComplete as AntdAutoComplete } from 'antd';

import { Input } from '../input';

// TODO: setup babel-plugin-lodash
// https://github.com/lodash/babel-plugin-lodash 

/** 
 * Possible props are from at least three sources:
 * 1. https://ant.design/components/auto-complete/
 * 2. https://ant.design/components/select/
 * 3. https://www.npmjs.com/package/rc-select
 * 
 * The 3rd party components are nested as follows (approximately):
 * <AutoComplete>
 *  <Select>
 *    <RcSelect></RcSelect>
 *  </Select>
 * </AutoComplete>
 * 
 * and the props are passed down to this chain.
*/
export const AutoComplete = (props) => {

  return (
    <div className={styles.autoCompleteWrapper}>
      <AntdAutoComplete
        className={styles.autoComplete}
        dataSource={props.dataSource}
        onSelect={props.onSelect}
        onSearch={props.handleSearch}
        optionLabelProp={props.optionLabelProp}
        dropdownClassName={props.fixed ? styles.fixedDropdown : undefined}
      >
        <Input id={props.id} placeholder={props.placeholder} onKeyPress={props.handleKeyPress} />
      </AntdAutoComplete>
    </div>
  );
}
export default AutoComplete;
