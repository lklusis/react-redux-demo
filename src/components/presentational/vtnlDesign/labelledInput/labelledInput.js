import React, { Component } from 'react';
import styles from './labelledInput.less';
import { Input as AntdInput } from 'antd';
import ReactDOM from 'react-dom';
import { Input } from '../input';

import { uniqueId } from 'lodash/fp';

// TODO: setup babel-plugin-lodash
// https://github.com/lodash/babel-plugin-lodash
export class LabelledInput extends Component {

    static defaultProps = {
        children: <Input></Input> // set children in default props
    }

    // TODO: most likely this will have to go to the
    // redux store
    syncParentChildIds() {
        let id;

        const childId = this.props.children.props.id;
        if (childId) {
            id = childId;
        } else {
            id = uniqueId();
        }

        this.setState({ inputId: id });
    }

    componentWillMount() {
        this.syncParentChildIds();
    }

    render() {
        const children = React.cloneElement(this.props.children, {
            id: this.state.inputId
        });

        return (
            <div className={`${styles.inputRow} ${this.props.className}`}>
                <div className={styles.inputWrapper}>{children}</div>
                <div className={styles.labelWrapper}>
                    <label htmlFor={this.state.inputId} className={styles.label}>
                        {this.props.label}
                    </label>
                </div>
            </div>
        );
    }
}

export default LabelledInput;
