import React from 'react';
import Header from 'components/presentational/Header';

const Layout = ({ children, params, location }) => (
  <div>
    <Header params={params} location={location} />
    <div className="container">
      {children}
    </div>
  </div>
);

export default Layout;
