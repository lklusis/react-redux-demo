import React from 'react';
import { Link } from 'react-router';

import NavbarLink from 'components/presentational/NavbarLink';
import Profile from 'components/containers/ProfileNavbar';

const Header = ({ children, params, location }) => {

    const isActive = (key) => {
        return location.pathname.indexOf(key) !== -1;
    }

    return (
        <nav className="navbar navbar-default">
            <div className="container">

                {/* TODO: extract to new file as Logo  */}
                <div className="navbar-header">
                    <Link className="navbar-brand" to="/">SharedApp</Link>
                </div>
                <NavbarLink
                    title="Counter"
                    href="/localCounter"
                    active={isActive('localCounter')}
                />
                <NavbarLink
                    title="Quests"
                    href="/quests"
                    active={isActive('quests')}
                />
                <NavbarLink
                    title="Counter2"
                    href="/remoteCounter"
                    active={isActive('remoteCounter')}
                />
                <NavbarLink
                    title="About"
                    href="/about"
                    active={isActive('about')}
                />
                <Profile />
            </div>
        </nav>
    );
}

export default Header;