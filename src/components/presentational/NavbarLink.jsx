import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

const NavbarLink = (props) => {
  const { title, href, active, ...rest } = props

  return (
    <ul className="nav navbar-nav">
      <li className={active && 'active'}>
        <Link to={href} {...rest}>
          {title}
          {active && (
            <span className="sr-only">
              (current)
            </span>
          )}
        </Link>
      </li>
    </ul>
);}

NavbarLink.propTypes = {
  title: PropTypes.string,
  href: PropTypes.string,
  active: PropTypes.bool,
};

export default NavbarLink;
