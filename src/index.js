import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import App from './app';
import './styles/main.less';

import WebFont from 'webfontloader';

WebFont.load({
    google: {
        families: ['Open Sans:400', 'sans-serif']
    }
});


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
