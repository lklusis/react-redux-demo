import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { browserHistory } from 'react-router';
import './animates.less';

export const Animated = Page => {
 
  return props =>{

    const hist = browserHistory;
    return (
    <div className="page">
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName={browserHistory ? 'SlideIn' : 'SlideOut' }
      >        
        <Page {...props} />
      </ReactCSSTransitionGroup>
    </div>
    )
  }
};

export default Animated;
