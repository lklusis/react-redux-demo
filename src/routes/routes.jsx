import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Layout from 'components/presentational/Layout';
import Home from 'components/presentational/Home';
import Counter from 'components/containers/Counter';
import RemoteCounter from 'components/containers/RemoteCounter';
import About from 'components/presentational/About';

import privateRoute from './privateRoute';
import Login from 'components/containers/forms/LoginForm';
import Signup from 'components/containers/forms/SignupForm';
import { Quests } from 'components/containers/quests';
import { Quest } from 'components/containers/quest';
import { fetchUser } from 'modules/user';
import { Animated } from './animated';

export default function getRoutes(onLogout, store, client) {

    const logout = (nextState, replace, cb) => {
        onLogout();
        if (client) {
            client.resetStore();
        }
        replace('/');
        cb();
    };

    if (store) {//from client
        let token = localStorage.getItem('auth-token');
        if (token !== null) {
            store.dispatch(fetchUser());
        }
    }

    return (
        <Route path="/" component={Layout}>
            <IndexRoute component={Animated(Home)} />
            <Route name="Counter" path="localCounter" component={Animated(Counter)} />
            <Route name="RemoteCounter" path="remoteCounter" component={Animated(privateRoute(RemoteCounter))} />
            <Route name="Quests" path="quests" component={Animated(Quests)} />
            <Route name="Quest" path="quests/:id" component={Animated(Quest)} />
            <Route name="About" path="about" component={Animated(About)} />
            <Route name="Login" path="login" component={Animated(Login)} />
            <Route name="Register" path="register" component={Animated(Signup)} />
            <Route name="Logout" path="logout" onEnter={logout} />
        </Route>
    );
}