import {
    SIGN_IN, SIGN_IN_SUCCESS, SIGN_IN_FAILED,
    SIGN_UP, SIGN_UP_SUCCESS, SIGN_UP_FAILED,
    SIGN_OUT
} from './auth.types';

import { LOCATION_CHANGE } from 'react-router-redux';

const mapSignInFailedMessage = (error) => {
    const statusCode = error.response.status;

    if (statusCode >= 500) {
        return 'Oops. It seems that server has some issues. Please try again later';
    }

    if (statusCode >= 400 && statusCode < 500) {
        return 'Your credentials seems to be incorrect. Please try again.';
    }

    return 'Oops. Something went wrong. Please try again'
}

export default function (auth = { authenticated: false, token: null, logging: false, messageType: 'info', message: '' }, action) {
    switch (action.type) {
        case SIGN_IN:
            return Object.assign({}, auth, {
                logging: true,
                message: 'Logging...'
            })
        case SIGN_IN_SUCCESS:
            return Object.assign({}, auth, {
                logging: false,
                token: action.result.data,
                authenticated: true,
                message: ''
            })
        case SIGN_IN_FAILED:
            return Object.assign({}, auth, {
                logging: false,
                authenticated: false,
                message: mapSignInFailedMessage(action.error)
            })
        case SIGN_UP:
            return Object.assign({}, auth, {
                message: 'Signing up...'
            })
        case SIGN_UP_SUCCESS:
            return Object.assign({}, auth, {
                message: 'Sign up success'
            })
        case SIGN_UP_FAILED:
            return Object.assign({}, auth, {
                message: 'Sign up failed.'
            })
        case SIGN_OUT:
            return Object.assign({}, auth, {
                authenticated: false,
                token: null,
            })
        case LOCATION_CHANGE:
            return Object.assign({}, auth, {
                message: ''
            })
        default:
            return auth
    }

}