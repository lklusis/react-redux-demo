/// This file is following Ducks pattern
/// https://github.com/erikras/ducks-modular-redux
/// However, the actual implementation was moved to other files

/// Rules
/// A module...
/// 1. MUST export default a function called reducer()
/// 2. MUST export its action creators as functions
/// 3. MUST have action types in the form npm-module-or-app/reducer/ACTION_TYPE
/// 4. MAY export its action types as UPPER_SNAKE_CASE,
///    if an external reducer needs to listen for them,
///    or if it is a published reusable library

// Actions
export {
    SIGN_IN,
    SIGN_IN_SUCCESS,
    SIGN_IN_FAILED,
    SIGN_UP,
    SIGN_UP_SUCCESS,
    SIGN_UP_FAILED,
    SIGN_OUT
} from './auth.types'


// Reducer
export { default, default as reducer } from './auth.reducers';

// Action Creators


// side effects, only as applicable
// e.g. thunks, epics, etc
export {
    signInUser,
    signOutUser,
    signUpUser,
    redirectToLoginWithMessage
} from './auth.actions'