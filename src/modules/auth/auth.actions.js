import {
    SIGN_IN, SIGN_IN_SUCCESS, SIGN_IN_FAILED,
    SIGN_UP, SIGN_UP_SUCCESS, SIGN_UP_FAILED,
    SIGN_OUT
} from './auth.types';
import { fetchUser } from 'modules/user';
import client from 'axios';

import { browserHistory } from 'react-router'

export const signInUser = (username, password) => async (dispatch, getState) => {

    try {
        dispatch({ type: SIGN_IN });
        const response = await client.post('/signin', {
            username: username,
            password: password
        });

        dispatch({ type: SIGN_IN_SUCCESS, result: response })

        if (response.status === '200') {
            dispatch(fetchUser());
            localStorage.setItem('auth-token', getState().auth.token);
            if (getState().routing.locationBeforeTransitions) {
                const routingState = getState().routing.locationBeforeTransitions.state || {};
                browserHistory.push(routingState.nextPathname || '/');
            } else {
                browserHistory.push('/');
            }
        }
    } catch (error) {

        if (error.response.status >= 500) {
            // TODO: send log of this error to monitoring system
        }
        dispatch({ type: SIGN_IN_FAILED, error })
    }
}

export function signUpUser(username, password) {
    return {
        types: [SIGN_UP, SIGN_UP_SUCCESS, SIGN_UP_FAILED],
        promise: client => client.post('/signup', {
            username: username,
            password: password
        }),
        afterSuccess: (dispatch, getState, response) => {
            if (response.status === '200') {
                browserHistory.push('/login');
            }
        }
    };
}

export function signOutUser() {
    localStorage.removeItem('auth-token');
    return {
        type: SIGN_OUT
    }
}

export function redirectToLoginWithMessage() {
    return (dispatch, getState) => {
        if (getState().routing.locationBeforeTransitions) {
            const currentPath = getState().routing.locationBeforeTransitions.pathname;
            browserHistory.replace({ pathname: '/login', state: { nextPathname: currentPath } });
        } else {
            browserHistory.push('/login');
        }
    }
}