/// This file is following Ducks pattern
/// https://github.com/erikras/ducks-modular-redux
/// However, the actual implementation was moved to other files

/// Rules
/// A module...
/// 1. MUST export default a function called reducer()
/// 2. MUST export its action creators as functions
/// 3. MUST have action types in the form npm-module-or-app/reducer/ACTION_TYPE
/// 4. MAY export its action types as UPPER_SNAKE_CASE,
///    if an external reducer needs to listen for them,
///    or if it is a published reusable library

import destinationsAutocomplete from  './destinationsAutocomplete.reducers';

// Actions
export {
    FETCH_DESTINATIONS, FETCH_DESTINATIONS_FAILED, FETCH_DESTINATIONS_SUCCESS
} from './destinationsAutocomplete.types';


// Reducer

// TODO: think whether this is good approach
// To nest combined reducers does not seem like a bad idea
// https://github.com/reactjs/redux/issues/738
// however, the declarative manner 
// (i.e. the single place of reducers documentation)
// is gone
export default destinationsAutocomplete;

// Action Creators


// side effects, only as applicable
// e.g. thunks, epics, etc
export {
    fetchDestinations
} from './destinationsAutocomplete.actions'