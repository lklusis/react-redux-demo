
import {
    FETCH_DESTINATIONS, FETCH_DESTINATIONS_SUCCESS, FETCH_DESTINATIONS_FAILED
} from './destinationsAutocomplete.types';

import client from 'axios';


export const fetchDestinations = (request) => async (dispatch, getState) => {
    try {
        dispatch({ type: FETCH_DESTINATIONS });

        const response = await client.get(`https://www.greitai.lt/en/xhr2/search/suggestions/4/${request}/0/`);
        response.request = request; // @TODO hack

        dispatch({ type: FETCH_DESTINATIONS_SUCCESS, result: response })

    } catch (error) {
        dispatch({ type: FETCH_DESTINATIONS_FAILED, error })
    }
};
