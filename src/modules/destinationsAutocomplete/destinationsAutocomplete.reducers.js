import {
    FETCH_DESTINATIONS, FETCH_DESTINATIONS_SUCCESS, FETCH_DESTINATIONS_FAILED
} from './destinationsAutocomplete.types';


import _ from 'lodash';

export default function (destinations = {
    loading: false,
    suggestions: [],
    errorMessage: '',
    request: '' // @TODO hack - request data
}, action) {
    switch (action.type) {
        case FETCH_DESTINATIONS:
            return Object.assign({}, destinations, {
                loading: true,
                errorMessage: '',
                request: '',// @TODO hack
            });
        case FETCH_DESTINATIONS_SUCCESS:
            return Object.assign({}, destinations, {
                loading: false,
                suggestions: action.result.data
                    ? _.reduce(action.result.data, (list, item) => _.concat(list, item)) : [],
                errorMessage: '',
                request: action.result.request,// @TODO hack
            });
        case FETCH_DESTINATIONS_FAILED:
            return Object.assign({}, destinations, {
                loading: false,
                suggestions: [],
                errorMessage: 'Oops. Something went wrong.',
                request: '',// @TODO hack
            });
        default:
            return destinations
    }
}