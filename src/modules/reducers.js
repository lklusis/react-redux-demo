import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form'
import counter from './counter';
import auth from './auth';
import user from './user';
import quests from './quests';
import destinationsAutocomplete from './destinationsAutocomplete';

export default (apolloClient) => {
    return combineReducers({
        counter,
        auth,
        user,
        destinationsAutocomplete,
        routing,
        form: formReducer,
        quests: quests(),
        apollo: apolloClient.reducer()
    });
}