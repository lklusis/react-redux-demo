import { QUESTS } from './quests.mock'
import {
    FETCH_QUESTS, FETCH_QUESTS_SUCCESS, FETCH_QUESTS_FAILED,
    FETCH_QUEST, FETCH_QUEST_SUCCESS, FETCH_QUEST_FAILED
} from './quests.types';

import { browserHistory } from 'react-router'

const wait = (delay) => {
    return new Promise(resolve => setTimeout(resolve, delay));
}

export const selectQuest = (id) => async (dispatch, getState) => {
    browserHistory.push('quests/' + id);
}

export const fetchQuests = () => async (dispatch, getState) => {
    try {
        dispatch({ type: FETCH_QUESTS });
        //TODO fetch
        await wait(1000);
        const response = {
            data: QUESTS
        }

        // if (Math.round((Math.random() * 3) % 3) === 1) {
        //     throw new Error('DELETE THIS MOCK ERROR');
        // }

        dispatch({ type: FETCH_QUESTS_SUCCESS, result: response })

    } catch (error) {
        dispatch({ type: FETCH_QUESTS_FAILED, error })
    }
}

export const fetchQuest = (id) => async (dispatch, getState) => {
    try {
        dispatch({ type: FETCH_QUEST });

        const response = {};
        const existingQuest = getState().quests.list.questsList.find(q => q.id === id);
        
        if(existingQuest){
             response.data=existingQuest;                
        }else{
            //TODO fetch
            await wait(1000);
            response.data= QUESTS.find(q => q.id === id);
        }
        

        // if (Math.round((Math.random() * 3) % 3) === 1) {
        //     throw new Error('DELETE THIS MOCK ERROR');
        // }

        dispatch({ type: FETCH_QUEST_SUCCESS, result: response })

    } catch (error) {
        console.log(error);
        dispatch({ type: FETCH_QUEST_FAILED, error })
    }
}
