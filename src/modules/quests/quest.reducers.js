import {
    FETCH_QUEST, FETCH_QUEST_SUCCESS, FETCH_QUEST_FAILED
} from './quests.types';
import { SIGN_OUT } from 'modules/auth';

export default function (quest = { item: {}, loading: false, errorMessage: '' }, action) {
    switch (action.type) {
        case FETCH_QUEST:
            return Object.assign({}, quest, {
                loading: true,
                errorMessage: ''
            })
        case FETCH_QUEST_SUCCESS:
            return Object.assign({}, quest, {
                loading: false,
                item: action.result.data ? action.result.data : null,
                errorMessage: ''
            })
        case FETCH_QUEST_FAILED:
            return Object.assign({}, quest, {
                loading: false,
                item: {},
                errorMessage: 'Oops. Something went wrong. We could not retrieve your quests. Please try again'
            })
        case SIGN_OUT:
            return {}
        default:
            return quest
    }
}