export const QUESTS = [
    {
        id: '59e2858f4cb6d539f470ac46',
        name: 'Devotion of Mad Proffesor',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the'
        + ' 1500s, when an unknown printer took a galley of type and scrambled it'
        + ' to make a type specimen book.',
        photoUrl: 'https://avante.biz/wp-content/uploads/Raising-the-Flag-on-Iwo-Jima-Wallpapers/Raising-the-Flag-on-Iwo-Jima-Wallpapers-028.jpg',
    },
    {
        id: '59e3907f2e6d3c218c590e6e',
        name: 'Dates of our parents',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the'
        + ' 1500s, when an unknown printer took a galley of type and scrambled it'
        + ' to make a type specimen book.  It has survived not only five centuries,'
        + ' but also the leap into electronic typesetting, remaining essentially'
        + ' unchanged. It was popularised in the 1960s with the release of Letraset'
        + ' sheets containing Lorem Ipsum passages',
        photoUrl: 'https://i.pinimg.com/736x/71/0e/1d/710e1d97a5f962c62d271da5fccb696a--coffee-date-drink-coffee.jpg',
    },
    {
        id: '59e3936a51acd2306ce3eb54',
        name: 'Racing with the enemy',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the'
        + ' 1500s, when an unknown printer took a galley of type and scrambled it'
        + ' to make a type specimen book.',
        photoUrl: 'https://i.pinimg.com/236x/b7/83/f4/b783f44d2094bfb1f4d2707170dc656d--minimalist-movie-posters-minimalist-art.jpg'
    },
    {
        id: '59e398c351acd2306ce3eb56',
        name: 'The dancing city',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the'
        + ' 1500s, when an unknown printer took a galley of type and scrambled it'
        + ' to make a type specimen book.  It has survived not only five centuries,'
        + ' but also the leap into electronic typesetting, remaining essentially'
        + ' unchanged. It was popularised in the 1960s with the release of Letraset'
        + ' sheets containing Lorem Ipsum passages',
        photoUrl: 'http://www.festival.org/images/full/mulier0.jpg'
    },
    {
        id: '59e3946e51acd2306ce3eb55',
        name: 'The nations of where we live',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the'
        + ' 1500s, when an unknown printer took a galley of type and scrambled it'
        + ' to make a type specimen book.  It has survived not only five centuries,'
        + ' but also the leap into electronic typesetting, remaining essentially'
        + ' unchanged. It was popularised in the 1960s with the release of Letraset'
        + ' sheets containing Lorem Ipsum passages, and more recently with desktop'
        + ' publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        photoUrl: 'http://www.gibraltarolivepress.com/wp-content/uploads/2017/01/multicultural_London.jpeg'
    },
]