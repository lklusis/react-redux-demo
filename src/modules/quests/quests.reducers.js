import {
    FETCH_QUESTS, FETCH_QUESTS_SUCCESS, FETCH_QUESTS_FAILED
} from './quests.types';
import { SIGN_OUT } from 'modules/auth';

export default function (quests = { questsList: [], loading: false, errorMessage: '' }, action) {
    switch (action.type) {
        case FETCH_QUESTS:
            return Object.assign({}, quests, {
                loading: true,
                errorMessage: ''
            })
        case FETCH_QUESTS_SUCCESS:
            return Object.assign({}, quests, {
                loading: false,
                questsList: action.result.data ? action.result.data : null,
                errorMessage: ''
            })
        case FETCH_QUESTS_FAILED:
            return Object.assign({}, quests, {
                loading: false,
                questsList: [],
                errorMessage: 'Oops. Something went wrong. We could not retrieve your quests. Please try again'
            })
        case SIGN_OUT:
            return {}
        default:
            return quests
    }
}