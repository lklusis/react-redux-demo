import { ADD_COUNTER } from './counter.types';

export function addCount() {
    return { type: ADD_COUNTER };
}
