/// This file is following Ducks pattern
/// https://github.com/erikras/ducks-modular-redux
/// However, the actual implementation was moved to other files

/// Rules
/// A module...
/// 1. MUST export default a function called reducer()
/// 2. MUST export its action creators as functions
/// 3. MUST have action types in the form npm-module-or-app/reducer/ACTION_TYPE
/// 4. MAY export its action types as UPPER_SNAKE_CASE,
///    if an external reducer needs to listen for them,
///    or if it is a published reusable library

// Actions
export {
    ADD_COUNTER
} from './counter.types'


// Reducer
export { default, default as reducer } from './counter.reducers';

// Action Creators


// side effects, only as applicable
// e.g. thunks, epics, etc
export {
    addCount
} from './counter.actions'