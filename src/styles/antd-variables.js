
//check documented varialbes:
// "https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less",

const responsiveBreakpoints = {
    "screen-xs": "1px",
    "screen-sm": "480px",
    "screen-md": "768px",
    "screen-lg": "992px",
    "screen-xl": "1200px",
}

const variables =Object.assign({},responsiveBreakpoints) ;

module.exports = variables;
