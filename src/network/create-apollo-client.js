import { ApolloClient, addTypenameToDocument } from 'apollo-client';

// TODO: Migrate ApolloClient to the 1.0+ http://dev.apollodata.com/react/migration.html
export default options => new ApolloClient(Object.assign({}, {
  queryTransformer: addTypenameToDocument,
  dataIdFromObject: (result) => {
    if (result.id && result.__typename) { // eslint-disable-line no-underscore-dangle
      return result.__typename + result.id; // eslint-disable-line no-underscore-dangle
    }
    return null;
  },
  // shouldBatch: true,
}, options));
